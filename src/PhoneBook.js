import React from 'react';


class PhoneBook extends React.Component {

    constructor() {
        super();

        this.state = {
            name: "",
            number: "",
            email: "",
            phoneBook: [],
        }

        this.handleInputName = (event) => {
            this.setState({
                name: event.target.value
            })
        }
        this.handleInputEmail = (event) => {
            this.setState({
                email: event.target.value
            })
        }

        this.handleInputPhone = (event) => {
            this.setState({
                number: event.target.value
            })
        }

        this.addContact = () => {

            const newContact = {
                name: this.state.name,
                number: this.state.number,
                email: this.state.email
            }

            if (this.state.name === "" || this.state.number === "" || this.state.email === "") {
                alert('All fields are required.');
                return;
            }

            this.setState((prevState) => ({
                phoneBook: prevState.phoneBook.concat(newContact),
                name: "",
                email: "",
                number: ""

            }))

        }


    }

    render() {

        let form = null;
            form =
                (
                    <div className="container">
                        <form className="form" id="phonebook__form">
                            <div className="form-group">
                                <label htmlFor="name">Name: </label>
                                <input type="text" className="form-control" id="name" value={this.state.name} onChange={this.handleInputName}
                                       name="name"
                                       placeholder="Enter name"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email address</label>
                                <input type="email" className="form-control" value={this.state.email} onChange={this.handleInputEmail} name="email" id="email"
                                       aria-describedby="emailHelp"
                                       placeholder="Enter email"/>
                            </div>
                            <div className="form-group">
                                    <label htmlFor="phone">Phone</label>
                                    <input type="text" className="form-control" value={this.state.number} onChange={this.handleInputPhone} name="phone"
                                           id="phone"
                                           placeholder="Enter phone"/>
                                </div>
                                <button type="button" className="btn btn-primary" onClick={this.addContact}>Add</button>
                            </form>

                    </div>

                )


        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-4"></div>


                    <div className="col-md-4">
                        <div className="App">
                            <h2 className="header">PhoneBook</h2>


                            {form}

                            {this.state.phoneBook.map(contact =>
                                <div className="contacts">
                                    <div className="row">
                                        <div className="col">
                                            name
                                        </div>
                                        <div className="col-6">
                                            <h5>{contact.name}</h5>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            Email:
                                        </div>
                                        <div className="col-6">
                                            <h5>{contact.email}</h5>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            Phone
                                        </div>
                                        <div className="col-6">
                                            <h5>{contact.number}</h5>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                            )}
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

export default PhoneBook;